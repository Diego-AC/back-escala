FROM python:3.8.6-alpine3.12

RUN pip install flask

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app

CMD ["flask","run","--host=0.0.0.0"]
