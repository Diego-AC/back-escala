from flask import Flask

def register_blueprint(app):
    from .endpoints import blueprint as enterprises
    app.register_blueprint(enterprises)



def runserver():
    app = Flask(__name__)

    register_blueprint(app)

    return app
