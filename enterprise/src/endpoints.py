from flask import Blueprint


blueprint = Blueprint('enterprises',__name__)

@blueprint.route('/')
def index():
    return '<h1>Microservicios Empresas</h1>'
