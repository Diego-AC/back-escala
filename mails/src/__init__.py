from flask import Flask
from flask_mail import Mail
from src.configs import ConfigMail


mail = Mail()

def register_blueprint(app):
    from .endpoints import blueprint as mails
    app.register_blueprint(mails)



def runserver():
    app = Flask(__name__)
    app.config.from_object(ConfigMail)

    mail.init_app(app)
    register_blueprint(app)

    return app
