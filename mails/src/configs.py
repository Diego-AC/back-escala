import os

class ConfigMail:
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = os.environ.get('NAME_EMAIL')
    MAIL_DEFAULT_SENDER = os.environ.get('NAME_EMAIL')
    MAIL_PASSWORD = os.environ.get('PASSWORD_EMAIL')
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
