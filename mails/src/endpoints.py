from flask import Blueprint, request, render_template
from flask_mail import Message
from src import mail


blueprint = Blueprint('mails',__name__)

@blueprint.route('/')
def index():
    return '<h1>Microservicio de Correos Electrónicos</h1>'


@blueprint.route('/mail-verification-user/')
def verificationUser():
    datos = request.get_json()
    email = datos.get('email')
    name = datos.get('name')
    id = datos.get('id')
    url = 'http://localhost/verification-account-user/'+str(id)

    msg = Message('Verifica tu correo electronico',[email])
    msg.html = render_template('email.html', url=url, email=email, name=name)
    mail.send(msg)

    return 'OK',200
