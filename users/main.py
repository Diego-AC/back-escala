import sys
import unittest
import coverage
from project import runserver

COV = coverage.coverage(
    branch=True,
    include='project/*',
    omit=[
        'project/__init__.py',
        'project/_tests/*',
        'project/*/_tests/*'
    ]

)

COV.start()




app = runserver()


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('project', pattern='test_*.py')

    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        sys.exit(0)
    sys.exit(1)


@app.cli.command()
def cov():
    tests = unittest.TestLoader().discover('project', pattern='test_*.py')

    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        COV.stop()
        COV.save()
        COV.report(show_missing=True)
        COV.html_report()
        COV.erase()
        sys.exit(0)
    sys.exit(1)
