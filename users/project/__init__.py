from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from project.configs import DevConfig


db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()


def register_blueprint(app):
    from project.src.endpoints import blueprint as users
    app.register_blueprint(users)


def runserver():
    app = Flask(__name__)
    app.config.from_object(DevConfig)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    register_blueprint(app)

    return app
