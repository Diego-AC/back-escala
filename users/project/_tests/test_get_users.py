from project._tests.base_test import BaseTest


class TestGetUsers(BaseTest):
    def test_status_users(self):
        with self.client:
            response = self.client.get(
                '/users/health'
            )
            self.assertStatus(response, 200)

    def test_get_all_user(self):
        with self.client:
            response = self.client.get(
                '/users'
            )
            self.assertStatus(response, 200)
