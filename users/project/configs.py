class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'postgres-db',
        '5432',
        'users'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(BaseConfig):
    SQLALCHEMY_ECHO = True


class TestingConfig(BaseConfig):
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'postgres-db',
        '5432',
        'test_users'
    )
