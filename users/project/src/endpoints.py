# import requests
from flask import Blueprint, jsonify
# from project import db
from project.src.models import User
from project.src.serializers import user_schema


blueprint = Blueprint('users', __name__)


@blueprint.route('/users/health')
def index():
    return 'OK', 200


@blueprint.route('/users', methods=['GET'])
def get_all():
    users = User.query.all()
    return jsonify(user_schema.dump(users, many=True)), 200


# @blueprint.route('/register', methods=['POST'])
# def register():
#    data = request.get_json()
#    email = data.get('email')
#    count = User.query.filter_by(email=email).count()
#    if count > 0:
#        return jsonify({'detail': 'Ya esta registrado este correo'}), 409
#
#    user = user_schema.load(data)
#    db.session.add(user)
#    db.session.commit()
#
#    response = requests.get('http://mails:5000/mail-verification-user',
#                            json=user_schema.dump(user))
#    if response.status_code != 200:
#        return 'Error', 500
#    return user_schema.dump(user), 201


# @blueprint.route('/verification-account-user/<id>', methods=['GET'])
# def verificationUser(id):
#    user = User.query.filter_by(id=int(id)).first()
#    user.state = 'active'
#    db.session.commit()
#
#    return '<h5>Cuenta verificada</h5>', 200
