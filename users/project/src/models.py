from project import db


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(), nullable=False)
    lastName = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    state = db.Column(db.String(), default='pending')


class UserSkill(db.Model):
    __tablename__ = "userSkill"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    skils_id = db.Column(db.Integer, db.ForeignKey("skills.id"),
                         nullable=False)
    user = db.relationship("User", backref='users')
    skill = db.relationship("Skill", backref='skills')


class Skill(db.Model):
    __tablename__ = "skills"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name_skills = db.Column(db.String(), nullable=False)
